package com.bassiouny.chatbot.api;
import com.bassiouny.chatbot.model.MessageRequest;
import com.bassiouny.chatbot.model.MessageResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProjectApi {


    @POST("home")
    @FormUrlEncoded()
    Call<MessageResponse> sendMessage(@Field("question") String question);


}
