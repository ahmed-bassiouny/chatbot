package com.bassiouny.chatbot.api;

import android.util.Log;

import com.bassiouny.chatbot.api.RequestCallback;
import com.bassiouny.chatbot.api.RetrofitConfig;
import com.bassiouny.chatbot.model.MessageRequest;
import com.bassiouny.chatbot.model.MessageResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataCall {

    private String errorMsg = "Sorry,Please try again";

    public void sendMessage(String question,final RequestCallback<MessageResponse> callback){
        Call<MessageResponse> responseCall = RetrofitConfig.httpApiInterface.sendMessage(question);
        responseCall.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.code() == 200){
                    if (response.body() != null){
                        callback.success(response.body());
                    } else {
                        callback.failed(errorMsg);
                    }
                }else {
                    callback.failed(errorMsg);
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                callback.failed(t.getLocalizedMessage());
            }
        });

    }

}
