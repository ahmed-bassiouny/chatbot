package com.bassiouny.chatbot.api;


public interface RequestCallback<T> {
    void success(T t);
    void failed(String msg);
}
