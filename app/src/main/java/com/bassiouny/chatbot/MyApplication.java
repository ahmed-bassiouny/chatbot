package com.bassiouny.chatbot;

import android.app.Application;

import com.bassiouny.chatbot.api.RetrofitConfig;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        RetrofitConfig.initRetrofitConfig();
    }
}
