package com.bassiouny.chatbot.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MessageResponse {

    @SerializedName("answer")
    private String response;

    @SerializedName("replies")
    private List<Reply> replies;

    public MessageResponse() {
    }

    public MessageResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        if(response == null)
            response = "";
        return response;
    }

    public List<Reply> getReplies() {
        if (replies == null)
            replies = new ArrayList<>();
        return replies;
    }
}
