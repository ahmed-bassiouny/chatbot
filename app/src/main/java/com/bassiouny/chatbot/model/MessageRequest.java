package com.bassiouny.chatbot.model;

import com.google.gson.annotations.SerializedName;

public class MessageRequest {

    @SerializedName("question")
    private String question;

    public MessageRequest(String question) {
        this.question = question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
