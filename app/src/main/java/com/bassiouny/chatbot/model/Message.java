package com.bassiouny.chatbot.model;

public class Message {

    private String txt;
    private boolean isUserMsg;

    public Message(String txt, boolean isUserMsg) {
        this.txt = txt;
        this.isUserMsg = isUserMsg;
    }

    public String getTxt() {
        return txt;
    }

    public boolean isUserMsg() {
        return isUserMsg;
    }
}
