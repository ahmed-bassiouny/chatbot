package com.bassiouny.chatbot.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Reply {

    @SerializedName("stringValue")
    private String reply;

   public String getReply(){
       if (reply == null)
           reply = "";
       return reply;
   }
}
