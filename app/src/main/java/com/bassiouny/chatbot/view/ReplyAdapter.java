package com.bassiouny.chatbot.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bassiouny.chatbot.R;
import com.bassiouny.chatbot.model.Message;
import com.bassiouny.chatbot.model.Reply;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.UserViewHolder> {



    private List<Reply> list;
    private IReplyAdapter adapter;
    public ReplyAdapter(IReplyAdapter adapter) {

        this.adapter = adapter;

    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            View itemViewAd = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_user_reply, parent, false);
            return new UserViewHolder(itemViewAd);
        }



    @Override
    public void onBindViewHolder(@NonNull UserViewHolder viewHolder, int i) {

        String item = list.get(i).getReply();
        viewHolder.tvReply.setText(item);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<Reply> items){
        this.list = items;
        notifyDataSetChanged();
    }



    public class UserViewHolder extends RecyclerView.ViewHolder {

        TextView tvReply;
        public UserViewHolder(View view) {
            super(view);
            tvReply = view.findViewById(R.id.tvReply);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapter.click(list.get(getAdapterPosition()).getReply());
                }
            });
        }
    }

    public interface IReplyAdapter{
        void click(String item);
    }
}
