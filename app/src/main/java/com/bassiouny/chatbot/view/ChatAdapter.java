package com.bassiouny.chatbot.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bassiouny.chatbot.R;
import com.bassiouny.chatbot.model.Message;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private int userText = 1;

    private List<Message> list;

    public ChatAdapter() {

        list = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == userText) {
            View itemViewAd = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_user_msg, parent, false);
            return new userViewHolder(itemViewAd);
        } else  {
            View itemViewAd = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_bot_msg, parent, false);
            return new botViewHolder(itemViewAd);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        Message item = list.get(i);
        if (viewHolder.getItemViewType() == userText) {
            userViewHolder holder = (userViewHolder) viewHolder;
            holder.tvMsg.setText(item.getTxt());
        } else {
            botViewHolder holder = (botViewHolder) viewHolder;
            if (item.getTxt().isEmpty()){
                holder.spinKit.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.GONE);
            }else {
                holder.spinKit.setVisibility(View.GONE);
                holder.tvMsg.setText(item.getTxt());
                holder.tvMsg.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        return list.get(position).isUserMsg() ? 1 : 0;
    }

    public void setItem(Message item) {
        this.list.add(0,item);
        notifyItemInserted(0);
    }

    public void removeItem() {
        this.list.remove(0);
        notifyItemRemoved(0);
    }



    public class userViewHolder extends RecyclerView.ViewHolder {

        TextView tvMsg;
        public userViewHolder(View view) {
            super(view);
            tvMsg = view.findViewById(R.id.tvMsg);
        }
    }


    public class botViewHolder extends RecyclerView.ViewHolder {

        TextView tvMsg;
        SpinKitView spinKit;

        public botViewHolder(View view) {
            super(view);
            tvMsg = view.findViewById(R.id.tvMsg);
            spinKit = view.findViewById(R.id.spinKit);
        }
    }

}
