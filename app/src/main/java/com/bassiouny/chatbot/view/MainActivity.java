package com.bassiouny.chatbot.view;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bassiouny.chatbot.R;
import com.bassiouny.chatbot.controller.MainController;
import com.bassiouny.chatbot.model.Message;
import com.bassiouny.chatbot.model.MessageResponse;

public class MainActivity extends AppCompatActivity implements MainController.IMainController, ReplyAdapter.IReplyAdapter {

    private RecyclerView recyclerView,recyclerViewReply;
    private ChatAdapter adapter;
    private ImageView ivSend;
    private EditText etMsg;

    private MainController controller;
    private ReplyAdapter replyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // init views
        findViews();
        // handle action
        actionButton();
    }

    private void actionButton() {
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etMsg.getText().toString().isEmpty()) {

                    sendMsg(etMsg.getText().toString());

                    // clear edit text
                    etMsg.setText("");

                }
            }
        });
    }

    private void findViews() {
        // find view by id
        recyclerView = findViewById(R.id.recyclerView);
        recyclerViewReply = findViewById(R.id.recyclerViewReply);
        recyclerViewReply.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        replyAdapter = new ReplyAdapter(this);
        recyclerViewReply.setAdapter(replyAdapter);
        etMsg = findViewById(R.id.etMsg);
        ivSend = findViewById(R.id.ivSend);

        // create objects
        adapter = new ChatAdapter();
        recyclerView.setAdapter(adapter);

        controller = new MainController();

        // set welcome message
        adapter.setItem(new Message(getString(R.string.welcom_message), false));
    }

    @Override
    public void result(MessageResponse msg) {
        // remove loading icon
        adapter.removeItem();
        // set response from server to list
        adapter.setItem(new Message(msg.getResponse(), false));
        // scroll to first position
        recyclerView.scrollToPosition(0);
        // make button enabled
        ivSend.setEnabled(true);
        // set replaies

        if (!msg.getReplies().isEmpty()){
            replyAdapter.setList(msg.getReplies());
            recyclerViewReply.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void click(String item) {
        sendMsg(item);
    }

    private void sendMsg(String msg){
        // set text in list
        adapter.setItem(new Message(msg, true));
        // send text to server
        controller.sendMessage(msg,MainActivity.this);
        // create message for loading icon
        adapter.setItem(new Message("", false));
        // scroll to first position
        recyclerView.scrollToPosition(0);
        // make button enabled
        ivSend.setEnabled(false);
        // hidden replies
        recyclerViewReply.setVisibility(View.GONE);
    }
}
