package com.bassiouny.chatbot.controller;

import com.bassiouny.chatbot.api.DataCall;
import com.bassiouny.chatbot.api.RequestCallback;
import com.bassiouny.chatbot.model.MessageResponse;

public class MainController {

    private DataCall call;

    public MainController() {
        call = new DataCall();
    }

    public void sendMessage(String msg, final IMainController controller){
        call.sendMessage(msg, new RequestCallback<MessageResponse>() {
            @Override
            public void success(MessageResponse response) {
                controller.result(response);
            }

            @Override
            public void failed(String msg) {
                controller.result(new MessageResponse(msg));
            }
        });
    }

    public interface IMainController{
        void result(MessageResponse response);
    }
}
